# Android NDK API Guidelines

[TOC]

## API Rules {#rules}

One of the difficulties in concrete rules is applying them to a platform that
was developed without strict guidelines from the beginning, so some of the
existing APIs may not adhere. In some cases, the right choice might be to go
with what is consistent with APIs in the same general area of the code, rather
than in the ideal rules laid out herein.

The rules are a work in progress and will be added to in the future as other
patterns emerge from future API reviews.

## Compatibility <a name="compatibility"></a>

More information on these build requirements can be found at
https://android.googlesource.com/platform/ndk/+/master/docs/PlatformApis.md

### Headers Must be C Compatible <a name="headers"></a>

Even if it is not expected that C is ever used directly, C is the common
denominator of (nearly) all FFI systems & tools. It also has a simpler stable
ABI story than C++, making it required for NDK’s ABI stability requirements.

1. **Wrap in `__BEGIN_DECLS` and `__END_DECLS` from `sys/cdefs.h`**

2. **Non-typedef’ed structs must be used as `struct type` not just `type`**

Prefer typedef’ing structs, see the Naming Conventions section for more details.

For APEX APIs, this requirement can be relaxed. The ABI exposed must still be C
(the header must be contained in an extern "C" block), but the header can make
use of the following C++ features:

3. Sized-enums which can then also be used as parameters & return values
   safely.

For example:

```c++ {.good}
enum Foo : int16_t { One, Two, Three };
Foo transformFoo(Foo param);
```

### APIs must be tagged with the API Level <a name="compatibilty-methods"></a>

Wrap new methods in `#if __ANDROID_API__ >= <api_level>` & `#endif` pairs

Mark methods with `__INTRODUCED_IN(<api_level>);`

Example:

```c++ {.good}
#if __ANDROID_API__ >= 30

binder_status_t AIBinder_getExtension(AIBinder* binder, AIBinder** outExt) __INTRODUCED_IN(30);

#endif
```

### Types & enums should be documented with the API level added <a name="compatibilty-types-enums"></a>

1. **Types & enum declaration MUST NOT be guarded by
   `#if __ANDROID_API__ >= <api_level>`**.  This makes opportunistic usage via
   dlsym harder to do.

2. Instead their **documentation should include what API level they were added**
   in  with a comment saying “Introduced in API `<number>`.”

### Export map must be tagged with the API level <a name="compatibilty-export-map"></a>

1. **Libraries must have a `<name>.map.txt` with the symbol being exported**

2. **Symbols must be marked with `# introduced=<api_level>`**
    - Can be either on each individual symbol or on a version block.

### NDK APIs must have CTS tests <a name="compatibilty-cts"></a>

1. All NDK APIs are required to be **covered by at least one CTS test**.

2. The CTS test **must be built against the NDK proper**
    1. No includes of framework/ paths
    2. Must set an `sdk_version` in the Android.bp (`LOCAL_SDK_VERSION` for
       Android.mk) for the test


## Documentation <a name="documentation"></a>

### APIs must be well documented <a name="compatibilty-cts"></a>

1. If error return codes are being used, **the possible errors must be listed**
   as well.

2. **Thread-safe and thread-hostile objects/methods must be called out explicitly**
   as such.

3. **Object lifetime must be documented** for anything that’s not a standard
   new/free pair.

4. If ref counting is being used, **methods that acquire/release a reference**
   must be documented** as such.

## ABI stability guidelines <a name="abi-stability"></a>

### Prefer opaque structs <a name="opaque-structs"></a>

Opaque structs allow for size changes more naturally and are generally less
fragile.

An exception to this is if the type is inherently fixed. For example ARect is
not an opaque struct as a rectangle is inherently 4 fields: left, top, right,
bottom. Defining an HTTP header as a
`struct { const char* key; const char* value; }` would also be appropriate,
as HTTP headers are inherently fixed.

### malloc/free must come from the same build artifact <a name="malloc-free"></a>

Different build artifacts may, and often do, have different implementations
of malloc/free.

For example: Chrome may opt to use a different allocator for all of Chrome’s
code, however the NDK libraries it uses will be using the platform’s
default allocator. These may not match, and cannot free memory allocated
by the other.

1. **If a library allocates something, such as an opaque struct, it must also
provide a free function**.

2. **If a library takes ownership of an allocation, it must also take the free
function**.

### Constants are forever <a name="constants-are-forever"></a>

If a header defines an enum or constant, that value is forever.

1. For defined steppings in a range (such as priority levels or trim memory
   levels), **leave gaps in the numberings for future refinement**.

2. For return codes, **have static_asserts or similar to ensure the values are
   never accidentally changed**.

3. For configuration data, such as default timeouts, **use a getter method or an
   extern variable instead**.

### Prefer fixed-size types <a name="fixed-size-types"></a>

Primitive types like long can have varying sizes. This can lead to issues on
32-bit vs. 64-bit builds.

1. In general, **prefer instead the fixed-size types `int32_t`, `int64_t`,
   etc...**

2. **For counts of things, use `size_t`**

3. **If libc has an existing preference, use that instead** (eg, use pid_t if
   you’re taking a process ID)

4. **Use `int32_t` or `int64_t` for enum params/returns.**
   - The backing size of an enum is technically up to the compiler.
       As such, even if a parameter or return value represents an enum use
       instead a fixed-type like `int32_t`.
   - While annoying, a C++ wrapper can also trivially fix this. The
      compatibility risks are not worth it otherwise.

5. **Avoid `off_t`.**

    - The size of an `off_t` can vary based on the definition of
      [`_FILE_OFFSET_BITS`.](https://android.googlesource.com/platform/bionic/+/master/docs/32-bit-abi.md#32_bit-and)
      API MUST NOT use `off_t` and MUST use `off64_t` instead.


# API Design Guidelines <a name="design-guidelines"></a>

## Naming conventions <a name="naming"></a>

1. **Prefer `AClassName` for the type**

2. **Typedef by default**, for example:

  ```c++ {.good}
  struct AIBinder;
  typedef struct AIBinder AIBinder;
  ```

3. **Class methods should follow `AClassName_methodName` naming**

4. **Callbacks should follow a `AClassName_CallbackType` naming convention.**

5. **“Nested” classes should also follow a `AClassName_SubType` naming
   convention**

## JNI <a name="jni"></a>

### JNI interop methods should exclusively be in the NDK <a name="jni-ndk"></a>

As in, always do `AObject_fromJava(JNIEnv, jobject)` in the NDK rather
than having a `long Object#getNativePointer()` in the SDK.

Similarly do instead `jobject AObject_toJava(JNIEnv, AObject*)` in the
NDK rather than `new Object(long nativePtr);` in the SDK.

It is **recommended to have JNI interop APIs**.

### Java objects and native objects should use separate respective lifecycles <a name="jni-lifecycles"></a>

To the extent possible, the **Java objects and the native objects
should have lifecycles native to their respective environments & independent
of the other environment**.

That is, if a native handle is created from a Java object then the
Java object’s lifecycle should not be relevant to the native handle.
Similarly, if a Java object is created from a native object the native
object should not need to out-live the Java one.


Typically this means both the NDK & Java types should sit on a ref-count system
to handle this if the underlying instance is shared.

If the interop just does a copy of the data (such as for a trivial type),
then nothing special needs to happen.

Exceptions can be made if it’s impractical for the underlying type to be
referenced counted and it’s already scoped to a  constrained lifecycle.
For example, AParcel_fromJavaParcel adopts the lifecycle of the jobject and
as such does not have a corresponding free. This is OK as the lifecycle of a
Parcel is already scoped to the duration of a method call in general anyway,
so a normal JNI LocalRef will have suitable lifetime for typical usage.

### JNI interop APIs should be in their own header with a trailing `_jni` suffix. <a name="jni-lifecycles"></a>

**JNI interop APIs should be in their own header with a trailing `_jni` suffix.**

Example:
[`asset_manager.h`](https://cs.android.com/android/platform/superproject/+/master:frameworks/native/include/android/asset_manager.h)
and
[`asset_manager_jni.h`](https://cs.android.com/android/platform/superproject/+/master:frameworks/native/include/android/asset_manager_jni.h)

This helps apps to keep a clean JNI layer in their own code

### Errno values should not be propagated across the JNI boundary <a name="jni-errno"></a>

Errno values, such as EINVAL, are only constant for a given arch+abi
combination. As such, **they should not be propagated across the JNI boundary**
as specific literal numbers as the Java-side would lose its arch/abi
portability. Java code can, however, use the OsConstants class to refer to
errno values. As in, if AFOO_ERROR_BAR is defined as being EINVAL, then it
must only be referred to by EINVAL and not by the literal constant 22.


## Error Handling <a name="error-handling"></a>

### Methods that cannot fail {.numbered}

**If a method cannot fail, the return type should simply be the output
or void if there is no output.**

### allocation/accessor methods {.numbered}

**For allocation/accessor methods where the only meaningful failure is out of memory or similar, return the pointer and use NULL for errors.**

Example: only failure is `ENOMEM`: `AMediaDataSource* AMediaDataSource_new();`

Example: only failure is not set: `ALooper* ALooper_forThread();`

### Methods with non-trivial error possibilities {.numbered}

1. For methods with a non-trivial error possibility or multiple unique
   error types, **use an error return value and use an output parameter to
   return any results**

4. For APIs where the only error possibility is the result of a trivial check,
   such as a basic getter method where the only failure is a nullptr, do not
   introduce an error handling path but instead abort on bad parameters.

```c++ {.good}
size_t AList_getSize(const AList*);
```

```c++ {.bad}
status_t AList_getSize(const AList*, const size_t* outSize);
```


### Include a quality abort message {.numbered}

For example, in
[system_fonts.cpp](https://cs.android.com/android/platform/superproject/+/master:frameworks/base/native/android/system_fonts.cpp;l=356;drc=01709c7469b59e451f064c266bbe442e9bef0ab4):

```c++ {.good}
bool AFont_isItalic(const AFont* font) {
    LOG_ALWAYS_FATAL_IF(font == nullptr, "nullptr passed as font argument");
    return font->mItalic;
}
```


### Error return types should be stringifiable <a name="error-return-types"></a>

1. **Error return types should have a `to_string` method to stringify them.**

2. **The returned strings should be constants.**

3. **Invalid inputs to the stringify method should return nullptr.**



## Callbacks <a name="callbacks"></a>

### Callbacks should be a bare function pointer. <a name="callbacks-func-pointer"></a>

### Callbacks should take a `void*` for caller-provided context. <a name="callbacks-void"></a>

### Use “callback(s)” instead of “listener(s)”  <a name="callbacks-not-listeners"></a>

### For single callback APIs, use `setCallback` terminology <a name="callbacks-setcallback"></a>

**If the API only allows for a single callback to be set, use
“setCallback” terminology**

In such a case, the `void*` must be passed to both the setCallback method
as well as passed to the callback on invoke.

**To clear the callback, allow `setCallback` to take `NULL` to clear.**

### For multiple callback APIs, use `register`/`unregister` terminology <a name="callbacks-register-unregister"></a>

**If the API allows for multiple callbacks to be set, use register/unregister
terminology**

In such a case, the `void*` is needed on registerCallback, invoke, and
unregisterCallback.

**Register & unregister must use the pair of function pointer + void* as
the unique key**. As in, it must support registering the same function pointer
with different void* userData.

## Nullability <a name="nullability"></a>

### Document and use `_Nonnull` or `_Nullable` <a name="nullable"></a>

Document parameters & return values with `_Nonnull` or `_Nullable` as
appropriate.

These are defined in clang, see
https://clang.llvm.org/docs/AttributeReference.html#nullability-attributes

## Use `Const` <a name="const"></a>

### Use `const` when appropriate <a name="use-const"></a>

For example if a method is a simple getter for an opaque type, the struct
pointer argument should be marked `const`.

Example:

```c++ {.good}
size_t AList_getSize(const AList*);
```

```c++ {.bad}
size_t AList_getSize(AList*);
```

## AFoo_create vs. AFoo_new <a name="create-vs-new"></a>

### Prefer `_create` over `_new` <a name="prefer-new"></a>

Prefer `_create` over `_new` as it works better with `_createFrom`
specializations

### For destruction use `_destroy` <a name="destroy"></a>

## Reference counting <a name="ref-counting"></a>

### Prefer `_acquire`/`_release` over `_inc`/`_dec` for ref count naming <a name="ref-count-naming"></a>


